/*
    SPDX-FileCopyrightText: 2022 Weng Xuetian <wegnxt@gmail.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
// Generated from emoji-test.txt
#include "emojicategory.h"

const QStringList &getCategoryNames()
{
    static const QStringList names = {
        QString::fromUtf8("Smileys and Emotion"),
        QString::fromUtf8("People and Body"),
        QString::fromUtf8("Component"),
        QString::fromUtf8("Animals and Nature"),
        QString::fromUtf8("Food and Drink"),
        QString::fromUtf8("Travel and Places"),
        QString::fromUtf8("Activities"),
        QString::fromUtf8("Objects"),
        QString::fromUtf8("Symbols"),
        QString::fromUtf8("Flags"),
    };
    return names;
}
