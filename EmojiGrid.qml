import QtQuick 2.15
import QtQuick.Controls 2.15
import org.kde.plasma.emoji 1.0

GridView {
    id: emojiView
    cellWidth: 40
    cellHeight: 40

    model: EmojiModel {}

    delegate: Label {
        width: emojiView.cellWidth
        height: emojiView.cellHeight

        font.pointSize: 25
        font.family: 'emoji'
        text: model.display
    }
}
