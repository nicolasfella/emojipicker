/*
    SPDX-FileCopyrightText: 2019 Aleix Pol Gonzalez <aleixpol@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "emojierplugin.h"

#undef signals
#include "emojidict.h"
#include <QClipboard>
#include <QGuiApplication>
#include <QSortFilterProxyModel>
#include <QStandardPaths>
#include <qqml.h>

int AbstractEmojiModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_emoji.size();
}

QVariant AbstractEmojiModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index,
                    QAbstractItemModel::CheckIndexOption::IndexIsValid | QAbstractItemModel::CheckIndexOption::ParentIsInvalid
                        | QAbstractItemModel::CheckIndexOption::DoNotUseParent)
        || index.column() != 0)
        return {};

    const auto &emoji = m_emoji[index.row()];
    switch (role) {
    case Qt::DisplayRole:
        return emoji.content;
    case Qt::ToolTipRole:
        return emoji.description;
    case CategoryRole:
        return emoji.categoryName();
    case AnnotationsRole:
        return emoji.annotations;
    }
    return {};
}

EmojiModel::EmojiModel()
{
    const QHash<QString, QString> specialCases{{QLatin1String{"zh-TW"}, QLatin1String{"zh_Hant"}}, {QLatin1String{"zh-HK"}, QLatin1String{"zh_Hant_HK"}}};
    QLocale locale;
    QStringList dicts;
    auto bcp = locale.bcp47Name();
    bcp = specialCases.value(bcp, bcp);
    bcp.replace(QLatin1Char('-'), QLatin1Char('_'));

    const QString dictName = QLatin1String{"plasma/emoji/"} + bcp + QLatin1String{".dict"};
    const QString path = QStandardPaths::locate(QStandardPaths::GenericDataLocation, dictName);
    if (!path.isEmpty()) {
        dicts << path;
    }

    for (qsizetype underscoreIndex = -1; (underscoreIndex = bcp.lastIndexOf(QLatin1Char('_'), underscoreIndex)) != -1; --underscoreIndex) {
        const QString genericDictName = QLatin1String{"plasma/emoji/"} + bcp.left(underscoreIndex) + QLatin1String{".dict"};
        const QString genericPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, genericDictName);

        if (!genericPath.isEmpty()) {
            dicts << genericPath;
        }
    }

    // Always fallback to en, because some annotation data only have minimum data.
    const QString genericPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("plasma/emoji/en.dict"));
    dicts << genericPath;

    if (dicts.isEmpty()) {
        qWarning() << "could not find emoji dictionaries." << dictName;
        return;
    }

    QSet<QString> categories;
    EmojiDict dict;
    // We load in reverse order, because we want to preserve the order in en.dict.
    // en.dict almost always gives complete set of data.
    for (auto iter = dicts.crbegin(); iter != dicts.crend(); ++iter) {
        dict.load(*iter);
    }
    m_emoji = std::move(dict.m_emojis);
    for (const auto &emoji : m_emoji) {
        categories.insert(emoji.categoryName());
    }
    m_categories = categories.values();
    m_categories.sort();
}

QString EmojiModel::findFirstEmojiForCategory(const QString &category)
{
    for (const Emoji &emoji : m_emoji) {
        if (emoji.categoryName() == category)
            return emoji.content;
    }
    return {};
}

#include "emojierplugin.moc"
