#include <QApplication>
#include <QQmlApplicationEngine>

#include "emojierplugin.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<EmojiModel>("org.kde.plasma.emoji", 1, 0, "EmojiModel");

    engine.load(QStringLiteral("qrc:/emojier.qml"));

    return app.exec();
}
