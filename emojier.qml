import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow
{
    visible: true

    Button {
        id: button
        text: "Load"
        onClicked: {loader.source = ""; loader.setSource(Qt.resolvedUrl("EmojiGrid.qml"))}
    }

    Loader {
        id: loader
        anchors {
            left: parent.left
            right: parent.right
            top: button.bottom
            bottom: parent.bottom
        }
    }
}
